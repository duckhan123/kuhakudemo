import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {GameBoard} from './GameBoard';
import {Regiser, Register} from './components/register';
class App extends Component {
  constructor() {
    super();
    this.username = "";
    this.password = "";
    this.state = {
      time: "don't know",
      isLoggedIn: false
    };
    //subscribeTimer((time) => this.setState({ time }));
    this.onLoginClick = this.onLoginClick.bind(this);
    this.registerCallback=this.registerCallback.bind(this);
  }
  onLoginClick(event) {
    var user = {
      username: this.username.value,
      password: this.password.value
    };
    console.log(user);
    fetch('/user/login', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    })
    .then(res=>{
      return res.json();
    })
    .then(res => {
      console.log(res);
      if (res.status) {
        this.token = res.token;
        this.setState({ isLoggedIn: true });
      }
      else {
        alert(res.message);
      }
    });
  }
  registerCallback(result){
    alert('Register success.');
  }
  render() {
    var content = null;
    if (this.state.isLoggedIn) {
      content = (<GameBoard token={this.token}/>);
    }
    else {
      content = (
      <div>
      <form action="/user/login" method="post">
        <input type="text" name="username" ref={(username) => this.username = username} id="username" />
        <input type="password" name="password" ref={(password) => this.password = password} id="password" />
        <input type="button" value="Login" onClick={this.onLoginClick} />
      </form>
      <h1>Register</h1>
      <Register callback={this.registerCallback}/>
      </div>
      );
    }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        {content}
      </div>
    );
  }
}

export default App;
